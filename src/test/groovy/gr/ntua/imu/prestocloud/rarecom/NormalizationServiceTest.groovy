package gr.ntua.imu.prestocloud.rarecom

import gr.ntua.imu.prestocloud.rarecom.services.NormalizationService
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-01-31.
 */

class NormalizationServiceTest {

    NormalizationService service

    @Before
    void before(){

        service = new NormalizationService()

    }


    @Test
    public void range(){


        Assert.assertEquals(
                50.0,
                service.normalize(90,80,100),
                0.0
        )
    }

    @Test
    public void greaterThan(){

        Assert.assertEquals(
                -25,
                service.greaterThan(2500,3000,5000),
                0.0
        )
    }


    @Test
    public void lessThan(){


        Assert.assertEquals(
                -20,
                service.lessThan(2500,3000,500),
                0.0
        )
    }



}
