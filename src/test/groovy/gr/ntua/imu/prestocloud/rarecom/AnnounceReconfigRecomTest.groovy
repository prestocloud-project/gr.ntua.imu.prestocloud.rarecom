package gr.ntua.imu.prestocloud.rarecom

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.amqp.PrestoCloudPublisher
import gr.ntua.imu.prestocloud.rarecom.services.standalone.SituationHandlingService
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 *
 *
 * The following integration test is described in D6.6
 *
 * Title: SubscribeToEvents & AnnounceReconfigRecom Testing
 * Test Reference Code:  #ITC-07
 * Test Description:
 *
 * With this test the Resources Adaptation Recommender checks the proper integration with
 * the DIAFDRecom through the broker, for requesting the proper reconfiguration of a deployed application
 * (to be used with test #ITC-05).
 *
 *
 *
 * This unit test performs a round trip through the message broker and installs a Mock PrestoCloudPublisher
 * in order to intercept the message which is sent after the rules have been evaluated.
 *
 *
 * PrestoCloudPublisher -> (sends 'situations' event ) -> Broker -> PrestoCloudReceiver -> Situation Handling -> End
 *
 *
 */
@SpringBootTest
@RunWith(SpringRunner.class)
class AnnounceReconfigRecomTest {


    Logger logger = LoggerFactory.getLogger(AnnounceReconfigRecomTest.class)


    private PrestoCloudPublisher originalPublisher
    private PrestoCloudPublisher prestoCloudPublisher


    @Autowired
    SituationHandlingService situationHandlingService

    @Autowired
    Gson gsonizer



    @Test
    public void testSituation(){

        CountDownLatch countDownLatch = new CountDownLatch(1)

        String receivedTopic=null
        Object receivedMessage=null



        prestoCloudPublisher = new PrestoCloudPublisher() {
            @Override
            void publish(String topic, Object message) {

                receivedTopic = topic
                receivedMessage = gsonizer.fromJson(message, Object.class)
                logger.info("This message should be published {} {}",receivedTopic,receivedMessage)
                countDownLatch.countDown()

            }

        }

        originalPublisher=situationHandlingService.prestoCloudPublisher
        situationHandlingService.prestoCloudPublisher=prestoCloudPublisher

        originalPublisher.publish(
            'situations',
            gsonizer.toJson([

                "event":
                    [

                        "action"    : "cpu_high_lresp_low",
                        "variable": "lresp=0.022725353324235434,avg_cpu_perc",
                        "rule_id": "1",
                        "fragid"  : "1cc5Fragment",
                        "attributes"    : [
                            "avg_cpu_perc": 90,
                            "avg_mem_perc": 80
                        ],
                    ]
            ])
        )


        countDownLatch.await(5, TimeUnit.SECONDS)

        Assert.assertNotNull(receivedTopic)
        Assert.assertNotNull(receivedMessage)

        Assert.assertEquals("topology_adaptation.scale_out",receivedTopic)
        Assert.assertEquals("fragments_VideoTranscoder",receivedMessage.fragment_name)
        Assert.assertEquals(2,receivedMessage.delta_instances,0)


    }

}

