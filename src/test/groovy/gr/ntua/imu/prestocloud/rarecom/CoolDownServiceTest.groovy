package gr.ntua.imu.prestocloud.rarecom

import gr.ntua.imu.prestocloud.rarecom.config.ApplicationConfiguration
import gr.ntua.imu.prestocloud.rarecom.services.sdm.CoolDownService
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-12-12.
 */
class CoolDownServiceTest {

    CoolDownService coolDownService

    @Before
    void setup(){


        ApplicationConfiguration config = new ApplicationConfiguration()

        config.cooldown = 1000

        coolDownService= new CoolDownService()
        coolDownService.applicationConfiguration = config


    }



    @Test
    void firstTime(){

        Assert.assertFalse(coolDownService.shouldCoolDown('key'))


    }

    @Test
    void secondTime(){

        Assert.assertFalse(coolDownService.shouldCoolDown('key'))
        Assert.assertTrue(coolDownService.shouldCoolDown('key'))


    }

    @Test
    void timePast(){

        Assert.assertFalse(coolDownService.shouldCoolDown('key'))
        Thread.sleep(1000)
        Assert.assertFalse(coolDownService.shouldCoolDown('key'))
        Assert.assertTrue(coolDownService.shouldCoolDown('key'))

    }


}
