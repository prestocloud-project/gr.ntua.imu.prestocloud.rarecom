#!/usr/bin/env python
import pika
import time
import json
import random


SLEEP=.50
BROKER_HOST='rarecom.presto.imuresearch.eu' #okeanos


connection = pika.BlockingConnection(pika.ConnectionParameters(host=BROKER_HOST))
channel = connection.channel()

channel.exchange_declare(exchange='presto.cloud', exchange_type='topic', durable=True)


somedict = {
    "event":
        {   "type": "cpu_high_lresp_low",
            "variable":"lresp=0.022725353324235434,avg_cpu_perc",
            "resource":"fc4d:6255:6c98:b576:2fba:e464:8ba6:55e0",
            "attr":{
                "cpu":95,
                "mem":60
            },
            "fragid":"vt"}
}



message = json.dumps(somedict)
channel.basic_publish(exchange='presto.cloud',
                      routing_key='sdm.situation',
                      body=json.dumps(somedict))


connection.close()
