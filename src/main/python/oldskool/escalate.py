#!/usr/bin/env python
import pika
import time
import json
import random


SLEEP=.50
#BROKER_HOST='83.212.97.122' #okeanos
#BROKER_HOST='localhost' #okeanos
BROKER_HOST='rarecom.presto.imuresearch.eu' #okeanos


#connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
connection = pika.BlockingConnection(pika.ConnectionParameters(host=BROKER_HOST))
channel = connection.channel()

channel.exchange_declare(exchange='presto.cloud', exchange_type='topic', durable=True)

loop=500
current = 0.022725353324235434
value = current

while (loop !=0):

    value = value + (random.uniform(0.10, 0.80))


    if(value > 0.9):
        print ("Resetting\n")
        value = current



    somedict = {
        "event":
            {   "type": "cpu_high_lresp_low",
                "variable":"lresp=0.022725353324235434,avg_cpu_perc",
                "resource":"fc4d:6255:6c98:b576:2fba:e464:8ba6:55e0",
                "value":value,
                "fragid":"vt"}
    }


    message = json.dumps(somedict)
    channel.basic_publish(exchange='presto.cloud',
                          routing_key='sdm.situation',
                          body=json.dumps(somedict))

    print(" [x] Sent %r" % (message))

    time.sleep(SLEEP)



connection.close()
