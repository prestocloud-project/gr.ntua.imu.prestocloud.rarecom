#!/usr/bin/env python
import json
import pika

SLEEP=.50
BROKER_HOST='rarecom.presto.imuresearch.eu' #okeanos


connection = pika.BlockingConnection(pika.ConnectionParameters(host=BROKER_HOST))
channel = connection.channel()

channel.exchange_declare(exchange='presto.cloud', exchange_type='topic', durable=True)


somedict = {
    "topic":"topology_adaptation.scale_in",
    "deltaInstance":"2.0",
    "fragmentName":"fragments_VideoTranscoder",
    "config":2.0,
    "matched":"vt_down_2_2",
    "processed":"true"
}



message = json.dumps(somedict)
channel.basic_publish(exchange='presto.cloud',
                      routing_key='topology_adaptation.scale_in',
                      body=json.dumps(somedict))


connection.close()
