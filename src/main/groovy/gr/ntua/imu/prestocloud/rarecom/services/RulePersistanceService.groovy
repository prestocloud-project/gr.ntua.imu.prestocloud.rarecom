package gr.ntua.imu.prestocloud.rarecom.services

import com.google.gson.Gson
import org.apache.commons.io.IOUtils
import org.jtwig.JtwigModel
import org.jtwig.JtwigTemplate
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2018-12-21.
 */

@Service
class RulePersistanceService {

    Logger logger = LoggerFactory.getLogger(RulePersistanceService.class)

    @Autowired
    UiConfigService uiConfigService

    @Value('${application.rules}')
    int rules

    public File generateFromConfig( def configPath){


        def o  =  IOUtils.toString(new FileInputStream(configPath),'UTF-8')




        def object = new Gson().fromJson(o,List.class)
        def s = new StringBuilder()
        s.append("import gr.ntua.imu.prestocloud.rarecom.domain.SDMSituation\n")
        s.append("global gr.ntua.imu.prestocloud.rarecom.domain.ADFAction \$a\n")
        s.append("global gr.ntua.imu.prestocloud.rarecom.services.DistanceService \$d\n")


        def instances = rules
        object.each{
            config->

                uiConfigService.set(config)
                JtwigTemplate template = JtwigTemplate.classpathTemplate("templates/${config.fragment}_${config.action}.twig")

                for( int i = 1 ; i <=rules ; i++){

                    JtwigModel model = JtwigModel.newModel()
                            .with("config",config)
                            .with("value",i)
                            .with("instances", i)
                            .with( "isLast", i == rules)

                    String rule = template.render(model)

                    s.append(rule)
                    s.append()

                }

        }


        logger.debug(s.toString())

        def file = File.createTempFile("drools", "-dynamic.drl")
        file.createNewFile()
        IOUtils.write(s.toString(),new FileOutputStream(file),"UTF-8")


        return file




    }
}
