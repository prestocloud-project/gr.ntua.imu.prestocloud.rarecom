package gr.ntua.imu.prestocloud.rarecom.services

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.domain.FeedbackAction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import javax.sql.DataSource

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2018-12-21.
 */
@Service
class FeedbackActionService {


    Logger logger = LoggerFactory.getLogger(FeedbackActionService.class)

    @Autowired
    DataSource dataSource

    @Autowired
    Gson gsonizer

    SimpleJdbcInsert insert
    JdbcTemplate template

    @PostConstruct
    public post(){

        template = new JdbcTemplate(dataSource)
        insert = new SimpleJdbcInsert(dataSource)
                .withTableName("feedback_action")
                .usingGeneratedKeyColumns("id")

    }



    public void record(FeedbackAction action){

        logger.trace("Action processed {} ",gsonizer.toJson(action))

        insert.execute([
                "instance":action.instances,
                "increment":action.increment,
                "raw":gsonizer.toJson(action),
                "occurred":new Date()
                ]
        )

    }
}
