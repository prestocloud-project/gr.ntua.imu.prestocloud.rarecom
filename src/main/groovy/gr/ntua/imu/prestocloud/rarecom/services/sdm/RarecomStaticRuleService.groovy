package gr.ntua.imu.prestocloud.rarecom.services.sdm

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.config.ApplicationConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import javax.sql.DataSource

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-10-01.
 */
@Service
class RarecomStaticRuleService {

    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    DataSource dataSource

    @Autowired
    Gson gsonizer

    SimpleJdbcInsert insert
    SimpleJdbcInsert insertArch
    JdbcTemplate template

    @PostConstruct
    void post(){

        template = new JdbcTemplate(dataSource)
        insert = new SimpleJdbcInsert(dataSource)
            .withTableName("static_rules")
            .usingGeneratedKeyColumns("id")

        clear()

    }


    def getAll(String timeFrame) {

        return template.queryForList(
            "select * from static_rules " +
                " where " +
                " updated > NOW() - interval '${timeFrame}'",
        )

    }



    int getInstancesForSituation(def situation) {

        def instances = 1

        try{

            instances = template.queryForObject(
                "select instances from static_rules " +
                    " where fragid=? " +
                    " and action LIKE ? " +
                    " and zone =?" +
                    " order by updated desc limit 1",
                [situation.fragid, situation.action+'%',situation.zone] as Object[],
                Integer.class)

        }catch(EmptyResultDataAccessException e){

            instances=situation.zone

            insert.execute(
                [
                    "fragid":situation.fragid,
                    "action":situation.action,
                    "zone":situation.zone,
                    "instances":instances,
                    "updated": new Date()
                ]
            )

        }

        return instances

    }

    void updateInstancesForSituation(def instances, def situation) {

        template.update("update static_rules " +
                        " set instances=? , updated=?" +
                        " where fragid=? " +
                        " and action LIKE ? " +
                        " and zone =? ",
                            instances,
                            new Date(),
                            situation.fragid,
                            situation.action,
                            situation.zone
        )

    }

    void updateInstance(def instances, def id) {

        template.update("update static_rules " +
                        " set instances=? , updated=?" +
                        " where id=? " ,
                            instances,
                            new Date(),
                            id
        )

    }


    void clear(){
        template.update("delete from static_rules");
    }

}
