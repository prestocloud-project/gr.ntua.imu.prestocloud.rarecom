package gr.ntua.imu.prestocloud.rarecom.services.sdm

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.amqp.PrestoCloudPublisher
import gr.ntua.imu.prestocloud.rarecom.services.SituationHistoryService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 05/10/18.
 */

@Service
class SdmSituationHandlingService {

    Logger logger = LoggerFactory.getLogger(SdmSituationHandlingService.class)

    @Autowired
    PrestoCloudPublisher prestoCloudPublisher

    @Autowired
    Gson gsonizer

    @Autowired
    SituationHistoryService situationHistoryService

    @Autowired
    StandaloneFeedbackEvaluationService standaloneFeedbackEvaluationService

    @Autowired
    RarecomStaticRuleService rarecomStaticRuleService

    @Autowired
    TopologyService topologyService

    @Autowired
    CoolDownService coolDownService


    void handleMessage(Object message) {

        logger.debug("Handling {}", gsonizer.toJson(message))

        if (message == null) {
            return
        }


        if (!(message instanceof Collection)) {
            message = [message]
        }


        for (def o in message) {

            if (coolDownService.shouldCoolDown(o.event.rule_id)) {
                logger.debug("Cooling down event {} -> {} ", o.event.rule_id, o)
                continue
            }

            int lastDeltaInstance = rarecomStaticRuleService.getInstancesForSituation(o.event)


            def current = topologyService.getCurrentTopology(o.event.fragid)

            def max = current.max ==0 ? o.event.zone : current.max
            def min = current.min ==0 ? 1 : current.min


            int currentNumberOfInstances = 0

            if( o.event.fragid.contains("cloud") ){
                currentNumberOfInstances = current.cloud
            }

            if( o.event.fragid.contains("edge")){
                currentNumberOfInstances = current.edge
            }

            if( currentNumberOfInstances == 0){
                currentNumberOfInstances = current.total
            }



            int instancesDelta = lastDeltaInstance

            if(o.event.action == "scale_out"){

                int availableInstances = max - currentNumberOfInstances

                if ( instancesDelta > availableInstances ){
                    instancesDelta = availableInstances
                }

            }else{

                int availableInstances = currentNumberOfInstances- min
                if( instancesDelta < availableInstances){
                    instancesDelta = availableInstances
                }

            }

            if(instancesDelta <= 0){
                logger.warn("No need to send adaptations {}",instancesDelta)
                return
            }


            coolDownService.markRuleId(o.event.rule_id)

            prestoCloudPublisher.publish(
                "topology_adaptation.${o.event.action}",
                gsonizer.toJson([
                    "fragment_name"  : o.event.fragid,
                    "delta_instances": instancesDelta
                ])
            )


            situationHistoryService.insertSituation(o.event)
            standaloneFeedbackEvaluationService.evaluate(o.event)
        }


    }

}
