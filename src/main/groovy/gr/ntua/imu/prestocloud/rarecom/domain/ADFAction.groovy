package gr.ntua.imu.prestocloud.rarecom.domain

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 11/10/18.
 */

class ADFAction {

    String topic
    int deltaInstance
    String fragmentName
    Long timestamp
    double config
    String matched

    boolean  processed = false

    int getDeltaInstance() {
        return deltaInstance
    }

    void setDeltaInstance(int deltaInstance) {
        this.deltaInstance = deltaInstance
    }

    String getFragmentName() {
        return fragmentName
    }

    void setFragmentName(String fragmentName) {
        this.fragmentName = fragmentName
    }

    Long getTimestamp() {
        return timestamp
    }

    void setTimestamp(Long timestamp) {
        this.timestamp = timestamp
    }

    String getTopic() {
        return topic
    }

    void setTopic(String topic) {
        this.topic = topic
    }

    boolean getProcessed() {
        return processed
    }

    void setProcessed(boolean processed) {
        this.processed = processed
    }

    Long getConfig() {
        return config
    }

    void setConfig(double config) {
        this.config = config
    }

    void setMatched(String matched) {
        this.matched = matched
    }

    String getMatched() {
        return matched
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        ADFAction adfAction = (ADFAction) o

        if (processed != adfAction.processed) return false
        if (deltaInstance != adfAction.deltaInstance) return false
        if (fragmentName != adfAction.fragmentName) return false
        if (timestamp != adfAction.timestamp) return false
        if (topic != adfAction.topic) return false

        return true
    }

    int hashCode() {
        int result
        result = (topic != null ? topic.hashCode() : 0)
        result = 31 * result + (deltaInstance != null ? deltaInstance.hashCode() : 0)
        result = 31 * result + (fragmentName != null ? fragmentName.hashCode() : 0)
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0)
        result = 31 * result + (processed ? 1 : 0)
        return result
    }
}
