package gr.ntua.imu.prestocloud.rarecom.config

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 26/10/18.
 */
@Configuration
class GsonConfig {


    @Bean
    public Gson gsonizer(){
        return new GsonBuilder().create()
    }
}
