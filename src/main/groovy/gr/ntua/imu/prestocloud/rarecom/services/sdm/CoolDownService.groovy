package gr.ntua.imu.prestocloud.rarecom.services.sdm;

import gr.ntua.imu.prestocloud.rarecom.config.ApplicationConfiguration
import org.slf4j.Logger
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 * <p>
 * Created by fotis on 2019-12-12.
 */
@Service
public class CoolDownService {

    Logger logger = LoggerFactory.getLogger(CoolDownService.class)

    @Autowired
    ApplicationConfiguration applicationConfiguration

    private Map<String, Date> marks = new LinkedHashMap<>();

    boolean shouldCoolDown(String ruleId) {

        // If it is the first time then down't cool down
        if (!marks.containsKey(ruleId)) {
            return false
        }


        // if we have marked it, then cool down
        if((new Date().getTime() - marks.get(ruleId).getTime()) > applicationConfiguration.cooldown){
            return false;

        }
        return true;

    }

    void markRuleId(String ruleId){
        logger.debug("Marking cooldown date for {} ",ruleId)
        marks.put(ruleId, new Date());
    }

    void clear(){
        marks = new LinkedHashMap<>()
    }
}
