package gr.ntua.imu.prestocloud.rarecom.amqp

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
public class PrestoCloudPublisherImpl implements PrestoCloudPublisher{

    Logger logger = LoggerFactory.getLogger(PrestoCloudPublisherImpl.class)

    private String EXCHANGE_NAME = "presto.cloud";

    @Autowired
    Channel publishChannel


    void publish(String topic, Object message){

        logger.debug("Publishing {} => {} ",topic,message)

        publishChannel.basicPublish(
                EXCHANGE_NAME,
                topic,
                new AMQP.BasicProperties.Builder()
                        .contentType("text/plain")
                        .deliveryMode(2).priority(1).build() ,
                message.getBytes()


        )


    }


}