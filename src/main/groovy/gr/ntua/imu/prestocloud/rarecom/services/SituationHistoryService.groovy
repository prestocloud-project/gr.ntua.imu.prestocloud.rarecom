package gr.ntua.imu.prestocloud.rarecom.services

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.config.ApplicationConfiguration
import org.apache.commons.codec.digest.DigestUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import javax.sql.DataSource

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 31/10/18.
 */
@Service
class SituationHistoryService {


    Logger logger = LoggerFactory.getLogger(SituationHistoryService.class)


    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    DataSource dataSource

    @Autowired
    Gson gsonizer

    SimpleJdbcInsert insert
    SimpleJdbcInsert insertArch
    JdbcTemplate template


    @PostConstruct
    void post(){

        template = new JdbcTemplate(dataSource)
        insert = new SimpleJdbcInsert(dataSource)
                        .withTableName("situation")
                        .usingGeneratedKeyColumns("id")


        insertArch = new SimpleJdbcInsert(dataSource)
                        .withTableName("archs")
                        .usingGeneratedKeyColumns("id")
    }


    void clear(){
        template.update("delete from situation");
    }


    void insertSituation(def sdmSituation){

        logger.trace("Inserting situation {} {} ",sdmSituation)
        def json = gsonizer.toJson(sdmSituation)
        insert.execute(
                [
                        "fragid":sdmSituation.fragid,
                        "type":sdmSituation.action,
                        "raw": json,
                        "occurred":new Date(),
                        "hash": DigestUtils.sha256Hex(json)
                ]
        )
    }

    def situationFrequency(def situation){

        return template.queryForObject(
                "select count(*) from situation " +
                        "where " +
                        "      type LIKE ? " +
                        "   and fragid=? " +
                        "   AND occurred > NOW() - interval '${applicationConfiguration.feedback.timeframe}'",
                [situation.action+"%",situation.fragid] as Object[],
                Integer.class
        )

    }

    def situationFrequency(def situation, def timeframe){

        return template.queryForObject(
                "select count(*) from situation " +
                        "where " +
                        "      type LIKE ? " +
                        "   and fragid=? " +
                        "   AND occurred > NOW() - interval '${timeframe}'",
                [situation.action+"%",situation.fragid] as Object[],
                Integer.class
        )

    }


    def situationsWithinTimeframe(def situation){

        return template.queryForList(
                "select * from situation " +
                        "where " +
                        "      type LIKE ? " +
                        "   and fragid=? " +
                        "   AND occurred > NOW() - interval '${applicationConfiguration.feedback.timeframe}'" +
                        "   order by occurred asc",
                [situation.action+'%',situation.fragid] as Object[]
        )

    }


    def situationsWithinTimeframe(def situation, def timeframe){

        return template.queryForList(
                "select * from situation " +
                        "where " +
                        "      type LIKE ? " +
                        "   and fragid=? " +
                        "   AND occurred > NOW() - interval '${timeframe}'" +
                        "   order by occurred asc",
                [situation.action+'%', situation.fragid] as Object[]
        )

    }



    def timeToSituation(def situation){

        def time = 0L
        try{

            def existing = template.queryForMap(
                    "select * from situation " +
                            " where fragid=? " +
                            " and type LIKE ? " +
                    "order by occurred DESC LIMIT 1",
                    situation.fragid,situation.action+'%')

            time = System.currentTimeMillis() - existing.occurred.getTime()

        }catch(EmptyResultDataAccessException e){

            logger.warn("No situation found for {} - {}",situation)
        }

        return time

    }


}
