package gr.ntua.imu.prestocloud.rarecom.services

import org.springframework.stereotype.Service

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-01-16.
 */
@Service
class UiConfigService {

    def config = [:] as Map


    public void set(def config){
        this.config[config.id]=config
    }

    public def get(id){
        return this.config[id]
    }
}
