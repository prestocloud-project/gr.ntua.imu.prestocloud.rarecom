package gr.ntua.imu.prestocloud.rarecom.amqp

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import gr.ntua.imu.prestocloud.rarecom.services.DroolsContainerService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.CoolDownService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.RarecomStaticRuleService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.SdmSituationHandlingService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.TopologyService
import gr.ntua.imu.prestocloud.rarecom.services.standalone.SituationHandlingService
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired

import java.util.function.BiConsumer

public class PrestoCloudReceiverBase implements BiConsumer<String, String> {

    Logger logger = LoggerFactory.getLogger(PrestoCloudReceiverBase.class)

    private final SituationHandlingService situationHandlingService
    private final SdmSituationHandlingService sdmSituationHandlingService
    private final RarecomStaticRuleService rarecomStaticRuleService
    private final TopologyService topologyService
    private final CoolDownService coolDownService

    Gson gsonizer
    DroolsContainerService droolsContainerService

    /**
     * Constructs a new instance and records its association to the passed-in channel.
     * @param channel the channel to which this consumer is attached
     */
    PrestoCloudReceiverBase(
        SituationHandlingService staticRulesHandleService,
        SdmSituationHandlingService sdmSituationHandlingService,
        RarecomStaticRuleService rarecomStaticRuleService,
        DroolsContainerService droolsContainerService,
        TopologyService topologyService,
        CoolDownService coolDownService

    ) {
        gsonizer = new GsonBuilder().create()
        this.situationHandlingService = staticRulesHandleService
        this.sdmSituationHandlingService = sdmSituationHandlingService
        this.rarecomStaticRuleService = rarecomStaticRuleService
        this.droolsContainerService = droolsContainerService
        this.topologyService = topologyService
        this.coolDownService = coolDownService

    }


    @Override
    void accept(String body, String topic) {

        try {

            def o = gsonizer.fromJson(body  , Object.class)

            logger.debug("Handling {} ",o)
            if (StringUtils.startsWithIgnoreCase(topic, "situations")) {
                sdmSituationHandlingService.handleMessage(o)
            }

            if (StringUtils.equalsIgnoreCase(topic, "sdm.situation")) {
                situationHandlingService.handleMessage(o)
            }


            if (StringUtils.equalsIgnoreCase(topic, "deployment.fragment_hosts")) {
                topologyService.handleMessage(o)
            }


            if (StringUtils.equalsIgnoreCase(topic, "rarecom.reset")) {

                if (StringUtils.equals(o.container, "static")) {

                    rarecomStaticRuleService.clear();
                    topologyService.clear();
                    droolsContainerService.resetStaticContainer()
                    coolDownService.clear();

                }
            }
            if (StringUtils.equalsIgnoreCase(topic, "undeployment.ack")) {

                rarecomStaticRuleService.clear();
                topologyService.clear();
                droolsContainerService.resetStaticContainer()
                coolDownService.clear();

            }

        }catch(Exception e){
            logger.error("Error consuming {} -> {} ",topic, body)
            e.printStackTrace()
        }


    }
}