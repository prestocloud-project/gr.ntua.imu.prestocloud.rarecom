package gr.ntua.imu.prestocloud.rarecom.config

import consumer.AmqpConsumer
import gr.ntua.imu.prestocloud.rarecom.amqp.PrestoCloudReceiverBase
import gr.ntua.imu.prestocloud.rarecom.services.DroolsContainerService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.RarecomStaticRuleService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.SdmSituationHandlingService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.TopologyService
import gr.ntua.imu.prestocloud.rarecom.services.standalone.SituationHandlingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import producer.AmqpProducer

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-10-29.
 */
//@Configuration
//@ConfigurationProperties("spring.rabbitmq")
class NissatechMQConfiguration {


    String host
    String username
    String password
    boolean isDNS = false

    @Autowired
    SituationHandlingService situationHandlingService
    @Autowired
    SdmSituationHandlingService sdmSituationHandlingService

    @Autowired
    RarecomStaticRuleService rarecomStaticRuleService

    @Autowired
    DroolsContainerService droolsContainerService

    @Autowired
    TopologyService topologyService



    @Bean
    public AmqpProducer producer(){

        AmqpProducer producer = new AmqpProducer(
            host,
            isDNS
        )

        producer.setUsernameAndPassword(username,password)

        return producer
    }



    @PostConstruct
    public void consumer(){

        def receiver = new PrestoCloudReceiverBase(
            situationHandlingService,
            sdmSituationHandlingService,
            rarecomStaticRuleService,
            droolsContainerService,
            topologyService
        )

        def situation = new AmqpConsumer(
            host,
            isDNS,
            "sdm.situation",receiver
        )

        def reset = new AmqpConsumer(
            host,
            isDNS,
            "rarecom.reset",
            receiver
        )


        def situations = new AmqpConsumer(
            host,
            isDNS,
            "situations.#",
            receiver
        )
        def topology = new AmqpConsumer(
            host,
            isDNS,
            "deployment.fragment_hosts",
            receiver
        )
        def undeployment = new AmqpConsumer(
            host,
            isDNS,
            "undeployment.ack",
            receiver
        )


        situation.setUsernameAndPassword(username,password)
        situation.subscribe("sdm.situation")

        reset.setUsernameAndPassword(username,password)
        reset.subscribe("rarecom.reset")

        situations.setUsernameAndPassword(username,password)
        situations.subscribe("situations.#")

        topology.setUsernameAndPassword(username,password)
        topology.subscribe("deployment.fragment_hosts")

        undeployment.setUsernameAndPassword(username,password)
        undeployment.subscribe("undeployment.ack")


    }

}
