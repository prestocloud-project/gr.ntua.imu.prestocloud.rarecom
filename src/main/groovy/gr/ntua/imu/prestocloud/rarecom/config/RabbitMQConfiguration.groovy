package gr.ntua.imu.prestocloud.rarecom.config

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import gr.ntua.imu.prestocloud.rarecom.amqp.PrestoCloudReceiverRabbit
import gr.ntua.imu.prestocloud.rarecom.services.DroolsContainerService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.CoolDownService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.RarecomStaticRuleService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.SdmSituationHandlingService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.TopologyService
import gr.ntua.imu.prestocloud.rarecom.services.standalone.SituationHandlingService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 04/10/18.
 */
@Configuration
@ConfigurationProperties("spring.rabbitmq")
class RabbitMQConfiguration {

    private String EXCHANGE_NAME = "presto.cloud";


    String host
    String username
    String password

    Logger logger = LoggerFactory.getLogger(RabbitMQConfiguration.class)


    @Autowired
    SituationHandlingService situationHandlingService
    @Autowired
    SdmSituationHandlingService sdmSituationHandlingService

    @Autowired
    RarecomStaticRuleService rarecomStaticRuleService

    @Autowired
    TopologyService topologyService


    @Autowired
    DroolsContainerService droolsContainerService

    @Autowired
    CoolDownService coolDownService
    @Bean
    public ConnectionFactory factory(){

        logger.info("Starting connection with username {} and a {} character password ", username, password == null ? 0: password.length())
        ConnectionFactory factory = new ConnectionFactory()
        factory.setUsername(username)
        factory.setPassword(password)
        factory.setHost(host)

        return factory
    }

    @Bean
    public Channel channel(ConnectionFactory factory){


        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()


        try {
            channel.exchangeDeclare("presto.cloud", "topic",true) //durable=true
        } catch (Exception e) {
            channel.exchangeDeclare("presto.cloud", "topic")
        }

        String queueName = channel.queueDeclare().getQueue()

        //** sdm.situation <<< --- complex
        //** situations <<< --- arches
        /**
         * {
         *   "event": {
         *     "rule_id": "Xs3UieyyCj"
         *     "timestamp": 1553785248
         *     "res_inst": "",
         *     "fragid": "KdkCcQkMNe",
         *     "zone": 2
         *     "action": "scale_out"
         *     "avg_cpu":  92
         *     "avg _ram": 71
         *   }
         * }
         */
        channel.queueBind(queueName, "presto.cloud", "sdm.situation")
        channel.queueBind(queueName, "presto.cloud", "rarecom.reset")
        channel.queueBind(queueName, "presto.cloud", "deployment.fragment_hosts")
        channel.queueBind(queueName, "presto.cloud", "situations")
        channel.queueBind(queueName, "presto.cloud", "situations.#")
        channel.queueBind(queueName, "presto.cloud", "undeployment.ack")

        def receiver = new PrestoCloudReceiverRabbit(
                channel,
                situationHandlingService,
                sdmSituationHandlingService,
                rarecomStaticRuleService,
                droolsContainerService,
                topologyService,
                coolDownService

        )
        channel.basicConsume(queueName, true, receiver)

        channel

    }


    @Bean
    Channel publishChannel(ConnectionFactory factory){

        def connection = factory.newConnection();
        Channel channel = connection.createChannel();

        try {
            channel.exchangeDeclare(EXCHANGE_NAME, "topic", true); //22-8-18 (last variable : durable )
        } catch (Exception e) {
            channel.exchangeDeclare(EXCHANGE_NAME, "topic", false); //22-8-18 (last variable : durable )
        }

        return channel

    }


}
