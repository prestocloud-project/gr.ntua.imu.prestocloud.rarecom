package gr.ntua.imu.prestocloud.rarecom.services.standalone

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.config.ApplicationConfiguration
import gr.ntua.imu.prestocloud.rarecom.domain.FeedbackAction
import gr.ntua.imu.prestocloud.rarecom.domain.FeedbackContext
import gr.ntua.imu.prestocloud.rarecom.domain.SDMSituation
import gr.ntua.imu.prestocloud.rarecom.services.DroolsContainerService
import gr.ntua.imu.prestocloud.rarecom.services.FeedbackActionService
import gr.ntua.imu.prestocloud.rarecom.services.FragmentService
import gr.ntua.imu.prestocloud.rarecom.services.SituationHistoryService
import gr.ntua.imu.prestocloud.rarecom.services.UiConfigService
import org.drools.core.impl.KnowledgeBaseImpl
import org.jtwig.JtwigModel
import org.jtwig.JtwigTemplate
import org.kie.api.io.ResourceType
import org.kie.api.runtime.KieSession
import org.kie.internal.builder.KnowledgeBuilder
import org.kie.internal.builder.KnowledgeBuilderFactory
import org.kie.internal.io.ResourceFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class FeedbackEvaluationService {

    Logger logger = LoggerFactory.getLogger(FeedbackEvaluationService.class)


    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    SituationHistoryService situationHistoryService

    @Autowired
    FeedbackActionService feedbackActionService


    @Autowired
    DroolsContainerService droolsContainerService


    @Autowired
    FragmentService fragmentService

    @Autowired
    UiConfigService uiConfigService

    @Autowired
    Gson gsonizer

    def evaluate(def adfAction, def situation) {


        if(!applicationConfiguration.feedback.enabled){
            logger.trace("Feedback service disabled")
            return
        }

        def split = adfAction.matched.split("_")

        def actionArea = split[0] + "_" + split[1]
        def time = situationHistoryService.timeToSituation(situation)
        def frequency = situationHistoryService.situationFrequency(situation)

        logger.trace("{} => {} " +
                "\n\tTime  {} " +
                "\n\tFrequency  {}",
                situation.fragid,
                situation.action,
                time,
                frequency
        )


        def context = new FeedbackContext()

        context.setTime(time)
        context.setFrequency(frequency)
        context.setTotalHosts(0)
        context.setCloudHosts(0)
        context.setEdgeHosts(0)
        context.setSituation(situation)


        def stats = fragmentService.getStatsForFragment(situation.fragid)

        if (stats != null) {
            context.setTotalHosts(stats.hosts.intValue())
            context.setCloudHosts(stats.cloud_hosts.intValue())
            context.setEdgeHosts(stats.edge_hosts.intValue())
        }

        logger.trace("Feedback context {} ",context)

        FeedbackAction action = new FeedbackAction()
        KieSession kieSession = droolsContainerService.getFeedbackContainer().newKieSession()
        kieSession.setGlobal('$a', action)
        kieSession.insert(context)
        kieSession.fireAllRules()
        kieSession.dispose()




        if (action.processed) {
            //alter rule

            feedbackActionService.record(action)

            changeSingleRule(adfAction, situation, actionArea, action, stats, split)


        } else {
            logger.trace("No feedback matched {}-{} ", time, gsonizer.toJson(situation))
        }


    }

    private void changeSingleRule(adfAction, SDMSituation situation, actionArea, FeedbackAction action, stats, split) {


        def staticContainer = droolsContainerService.getStaticContainer()
        def packages = staticContainer.getKieBase().getKiePackages()

        def alterRule = null

        packages.find {
            p ->

                p.getRules().each {
                    r ->

                        if (r.name == adfAction.matched) {

                            alterRule = [
                                "package": p.name,
                                "rule"   : r.name
                            ]
                        }
                }
        }

        if (alterRule == null) {
            logger.warn("No rules matched {}_{}", situation.action, situation.fragid)
            return
        }


        def situationsInTimeFrame = situationHistoryService.situationsWithinTimeframe(situation)


        def firstEvent = situationsInTimeFrame.first()
        logger.trace("The first one in the hour frequency is {} = {} ", firstEvent.occurred, firstEvent.type)
        alterRule.rule = firstEvent.type


        split = alterRule.rule.split("_");


        def base = staticContainer.getKieBase()
        logger.debug("Changing rule {} ", alterRule)
        logger.trace("Removing rule {}", base.getKiePackage(alterRule.package).getRules().size())
        base.removeRule(alterRule.package, alterRule.rule)
        logger.trace("Removing rule {}", base.getKiePackage(alterRule.package).getRules().size())


        JtwigTemplate template = JtwigTemplate.classpathTemplate("templates/${split[0]}_${split[1]}.twig")

        def instances = adfAction.deltaInstance + (split[1].equals("up") ? 1 : -1)

        if (action.increment > 0) {
            instances = stats.hosts * action.increment
        }


        JtwigModel model = JtwigModel.newModel()
            .with("instances", instances)
            .with("config", uiConfigService.get(split[2]))
            .with("value", split[3])

        String rule = template.render(model)

        StringBuilder s = new StringBuilder()
        s.append("import gr.ntua.imu.prestocloud.rarecom.domain.SDMSituation\n")
        s.append("global gr.ntua.imu.prestocloud.rarecom.domain.ADFAction \$a\n")
        s.append("global gr.ntua.imu.prestocloud.rarecom.services.DistanceService \$d\n")

        rule = s.toString() + rule

        logger.trace("Adding rule \n{}\n\n", rule)
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder(base)
        kbuilder.add(ResourceFactory.newByteArrayResource(rule.getBytes('UTF-8')), ResourceType.DRL);

        if (kbuilder.hasErrors()) {

            logger.error("Couldn't insert new rule")
            kbuilder.errors.each {
                error ->
                    logger.error("\t\t{}", error.getMessage())
            }

        }

        ((KnowledgeBaseImpl) staticContainer.getKieBase()).addPackages(kbuilder.getKnowledgePackages())
        logger.trace("Total rules size {}", base.getKiePackage(alterRule.package).getRules().size())
    }


}
