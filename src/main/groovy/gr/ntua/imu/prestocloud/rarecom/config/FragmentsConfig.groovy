package gr.ntua.imu.prestocloud.rarecom.config

import com.google.gson.Gson
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert

import javax.annotation.PostConstruct
import javax.sql.DataSource

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 08/11/18.
 */
@Configuration
class FragmentsConfig {



    Logger logger = LoggerFactory.getLogger(FragmentsConfig.class)


    @Autowired
    DataSource dataSource


    @Autowired
    Gson gsonizer

    SimpleJdbcInsert insert
    JdbcTemplate template


    @PostConstruct
    void post(){
        template = new JdbcTemplate(dataSource)

        def json = gsonizer.fromJson(
                new InputStreamReader(
                        FragmentsConfig.class.getResourceAsStream('/bootstrap/fragments.json')
                        , "UTF-8"

                ),
                Map.class
        )


        json.each{
            k,value ->
                try{
                    template.queryForMap("select * from fragment where name = ? limit 1", k)
                }catch(EmptyResultDataAccessException e){



                    template.update(
                            "insert into fragment values (default,?,?,?,?,null,now())",
                            k,
                            value["total"] as Integer,
                            value["cloud"] as Integer,
                            value["edge"] as Integer
                    )
                }



        }



    }




}
