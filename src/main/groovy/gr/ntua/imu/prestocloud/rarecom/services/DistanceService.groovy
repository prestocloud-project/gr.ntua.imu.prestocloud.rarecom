package gr.ntua.imu.prestocloud.rarecom.services

import gr.ntua.imu.prestocloud.rarecom.config.ApplicationConfiguration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-01-16.
 */
@Service
class DistanceService {

    @Autowired
    UiConfigService uiConfigService

    @Autowired
    LookupService lookupService

    @Autowired
    NormalizationService normalizationService

    @Autowired
    ApplicationConfiguration applicationConfiguration

    Logger logger = LoggerFactory.getLogger(DistanceService.class)
    public int distance(attributes, id){

        def config = uiConfigService.get(id)

        if(config == null || !config.containsKey("expressions")){
            logger.trace("Invalid configuration for id {} ",id)
            return -1
        }


        def akeys = new HashSet<>(attributes.keySet())

        for(def range : config.expressions){
            akeys.remove(range.metric)
        }

        if(akeys.size() !=0){
            logger.trace("Keys mismatch {} ",id)
            return -1
        }


        //check values
        def cont = true

        config.expressions.each{
            expresssion ->


                if(expresssion.operator.equals(">") && attributes[expresssion.metric] < Double.valueOf(expresssion.value)){
                    cont =false
                }

                if(expresssion.operator.equals("<") && attributes[expresssion.metric] > Double.valueOf(expresssion.value)){
                    cont =false
                }
        }

        if(!cont){
            logger.trace("Ranges do not match {} <> {}",attributes,config)
            return -1;
        }



        def newMap = [:]


        attributes.each{
            k,v ->

                /**
                 * we know for a fact this will be here
                 */
                def r = config.expressions.find{
                    e ->
                        e.metric == k
                }



                def vmin = applicationConfiguration.metrics[k].min
                def vmax = applicationConfiguration.metrics[k].max

                /**
                 * this value is unbounded
                 */
                if(vmax == null){
                    vmax = chebyshev()
                }



                if(">".equals(r.operator)) {
                    newMap[k] = normalizationService.greaterThan(v,r.value,vmax)
                }else{
                    newMap[k] = normalizationService.lessThan(v,r.value,vmax)

                }

        }

        def arches = lookupService.arches(attributes.size(), applicationConfiguration.rules)

        def d = 0.0
        newMap.each{
            k,v ->
                d += Math.pow(v,2)
        }


        def distance = Math.sqrt(d)


        def arch = 1
        for(def a : arches.arch){

            if(distance > a ){
                arch++
            }
        }

        if(arch > applicationConfiguration.rules){
            arch=applicationConfiguration.rules
        }

        logger.debug("Computed distance -> " +
                "\n\t Config Id {} " +
                "\n\t Arch: {} " +
                "\n\t Distance {}" +
                "\n\t Arches {}" +
                "\n\t Scalability Rule {}" +
                "\n\t Event {} " +
                "\n\t Normalized {} \n\n\n",id, arch,distance,arches, config.expressions, attributes,newMap)

        return arch


    }

    def int chebyshev(){
        return 10;
    }
}
