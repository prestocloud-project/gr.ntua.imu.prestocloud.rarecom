package gr.ntua.imu.prestocloud.rarecom.services.sdm

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.config.ApplicationConfiguration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import javax.sql.DataSource

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-10-01.
 */
@Service
class TopologyService {

    Logger logger = LoggerFactory.getLogger(TopologyService.class)
    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    DataSource dataSource

    @Autowired
    Gson gsonizer

    SimpleJdbcInsert insert
    JdbcTemplate template

    @PostConstruct
    void post() {

        template = new JdbcTemplate(dataSource)
        insert = new SimpleJdbcInsert(dataSource)
            .withTableName("topology")
            .usingGeneratedKeyColumns("id")

        clear()

    }


    def getAll(String timeFrame) {

        return template.queryForList(
            "select * from static_rules " +
                " where " +
                " updated > NOW() - interval '${timeFrame}'",
        )

    }


    def getCurrentTopology(def fragid) {

        def o = [
            "cloud": 0,
            "total": 0,
            "edge" : 0,
            "max"  : 0,
            "min"  : 0
        ]


        def fragids = template.queryForList("select distinct fragid from topology");


        def f = fragids.find{ t ->

            return fragid.startsWith(t.fragid)

        }

        if(!f){
            logger.error("Couldn't find topology for {} ",fragid)
            return o
        }

        o.each{
            k,v->
              try {

                  o[k] = template.queryForObject(
                      "select instances from topology " +
                      " where fragid=?  and type = ? " +
                      " order by updated DESC limit 1",
                      [f.fragid,k] as Object[],
                      Integer.class
                  )
              } catch (EmptyResultDataAccessException e) {
                  o[k] = 0
              }


        }

        return o

    }

    void updateTopology(def instances, def type, def fragid) {

        try {

            logger.debug("Inserting topology for {} -> {} / {} ", fragid, type, instances)
            insert.execute([
                "instances": instances,
                "updated"  : new Date(),
                "type"     : type,
                "fragid"   : fragid
            ])

        } catch (Exception e) {

            e.printStackTrace()
        }

    }

    void clear() {

        logger.debug("Clear topology")
        template.update("delete from topology")
    }


    def handleMessage(Object o) {

        o.each {
            k, v ->

                v.each {
                    t, d ->

                        updateTopology(
                            d, t, k
                        )

                }


                def current = getCurrentTopology(k)
                logger.debug("Current topology {} ",current)

        }


    }
}
