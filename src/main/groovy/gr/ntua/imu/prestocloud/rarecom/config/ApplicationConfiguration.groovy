package gr.ntua.imu.prestocloud.rarecom.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-02-07.
 */
@Component
@ConfigurationProperties(prefix='application')
class ApplicationConfiguration {

    Map metrics

    int rules

    FeedbackConfiguration feedback

    Long cooldown

    public static class FeedbackConfiguration{
        String timeframe
        boolean enabled

        int scaleInFrequency
        int scaleOutFrequency


    }
}
