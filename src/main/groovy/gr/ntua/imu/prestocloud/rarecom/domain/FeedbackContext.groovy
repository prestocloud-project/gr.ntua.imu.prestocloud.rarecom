package gr.ntua.imu.prestocloud.rarecom.domain

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 01/11/18.
 */
class FeedbackContext {

    def situation

    long maxTime
    long maxFrequency

    int frequency
    int totalHosts
    int cloudHosts
    int edgeHosts
    String act

    long time
    double averageLoad

    def getSituation() {
        return situation
    }

    void setSituation(situation) {
        this.situation = situation
        this.act= situation.action
    }

    String getAct() {
        return act
    }

    void setAct(String act) {
        this.act = act
    }

    long getTime() {
        return time
    }

    void setTime(long time) {
        this.time = time
    }

    double getAverageLoad() {
        return averageLoad
    }

    void setAverageLoad(double averageLoad) {
        this.averageLoad = averageLoad
    }

    int getTotalHosts() {
        return totalHosts
    }

    void setTotalHosts(int totalHosts) {
        this.totalHosts = totalHosts
    }

    int getCloudHosts() {
        return cloudHosts
    }

    void setCloudHosts(int cloudHosts) {
        this.cloudHosts = cloudHosts
    }

    int getEdgeHosts() {
        return edgeHosts
    }

    void setEdgeHosts(int edgeHosts) {
        this.edgeHosts = edgeHosts
    }

    int getFrequency() {
        return frequency
    }

    void setFrequency(int frequency) {
        this.frequency = frequency
    }

    long getMaxTime() {
        return maxTime
    }

    void setMaxTime(long maxTime) {
        this.maxTime = maxTime
    }

    long getMaxFrequency() {
        return maxFrequency
    }

    void setMaxFrequency(long maxFrequency) {
        this.maxFrequency = maxFrequency
    }

    @Override
    public String toString() {
        return "FeedbackContext{" +
            "situation=" + situation +
            ", frequency=" + frequency +
            ", totalHosts=" + totalHosts +
            ", cloudHosts=" + cloudHosts +
            ", edgeHosts=" + edgeHosts +
            ", time=" + time +
            ", averageLoad=" + averageLoad +
            '}';
    }
}
