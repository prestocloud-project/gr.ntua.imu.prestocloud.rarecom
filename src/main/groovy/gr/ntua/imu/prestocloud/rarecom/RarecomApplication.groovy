package gr.ntua.imu.prestocloud.rarecom

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class RarecomApplication {



	static void main(String[] args) {
		SpringApplication.run RarecomApplication, args
	}
}
