package gr.ntua.imu.prestocloud.rarecom.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-05-16.
 */
@Controller
class DefaultController {


    @RequestMapping(value = "/inform", method = RequestMethod.GET)
    def get(){
        return ""
    }


}
