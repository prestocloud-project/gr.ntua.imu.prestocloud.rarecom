package gr.ntua.imu.prestocloud.rarecom.domain

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 01/11/18.
 */
class FeedbackAction {

    String desc="none"

    int instances
    double increment

    boolean processed = false

    String getDesc() {
        return desc
    }

    void setDesc(String desc) {
        this.desc = desc
    }

    boolean getProcessed() {
        return processed
    }

    void setProcessed(boolean processed) {
        this.processed = processed
    }



    public int getInstances() {
        return instances
    }

    public  void setInstances(int instances) {
        this.instances = instances
    }

    double getIncrement() {
        return increment
    }

    void setIncrement(double increment) {
        this.increment = increment
    }

    @Override
    public String toString() {
        return "FeedbackAction{" +
            "desc='" + desc + '\'' +
            ", instances=" + instances +
            ", increment=" + increment +
            ", processed=" + processed +
            '}';
    }
}
