package gr.ntua.imu.prestocloud.rarecom.amqp


import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope
import gr.ntua.imu.prestocloud.rarecom.services.DroolsContainerService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.CoolDownService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.RarecomStaticRuleService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.SdmSituationHandlingService
import gr.ntua.imu.prestocloud.rarecom.services.sdm.TopologyService
import gr.ntua.imu.prestocloud.rarecom.services.standalone.SituationHandlingService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class PrestoCloudReceiverRabbit extends DefaultConsumer{



    Logger logger = LoggerFactory.getLogger(PrestoCloudReceiverRabbit.class)
    PrestoCloudReceiverBase base

    /**
     * Constructs a new instance and records its association to the passed-in channel.
     * @param channel the channel to which this consumer is attached
     */
    PrestoCloudReceiverRabbit(
        Channel channel,
        SituationHandlingService staticRulesHandleService,
        SdmSituationHandlingService sdmSituationHandlingService,
        RarecomStaticRuleService rarecomStaticRuleService,
        DroolsContainerService droolsContainerService,
        TopologyService topologyService,
        CoolDownService coolDownService

    ) {
        super(channel)
        base = new PrestoCloudReceiverBase(
            staticRulesHandleService,
            sdmSituationHandlingService,
            rarecomStaticRuleService,
            droolsContainerService,
            topologyService,
            coolDownService
        )

    }

    void handleDelivery(String consumerTag, Envelope envelope,
                        AMQP.BasicProperties properties, byte[] body) throws IOException {

        base.accept(new String(body),envelope.routingKey)

    }

}