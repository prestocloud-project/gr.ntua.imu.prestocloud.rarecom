package gr.ntua.imu.prestocloud.rarecom.amqp

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-10-01.
 */
interface PrestoCloudPublisher {

    void publish(String topic, Object message)

}

