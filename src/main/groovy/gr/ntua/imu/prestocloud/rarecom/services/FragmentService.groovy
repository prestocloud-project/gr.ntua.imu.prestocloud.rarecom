package gr.ntua.imu.prestocloud.rarecom.services

import com.google.gson.Gson
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import javax.sql.DataSource

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 08/11/18.
 */
@Service
class FragmentService {

    Logger logger = LoggerFactory.getLogger(SituationHistoryService.class)


    @Autowired
    DataSource dataSource


    @Autowired
    Gson gsonizer

    SimpleJdbcInsert insert
    JdbcTemplate template

    @PostConstruct
    void post(){

        template = new JdbcTemplate(dataSource)

    }


    def getStatsForFragment(String fragment){

        def returnFragment
        try{

            returnFragment = template.queryForMap("select * from fragment where name = ? order by stored desc limit 1", fragment)

        }catch(EmptyResultDataAccessException e){}

        return returnFragment
    }

}
