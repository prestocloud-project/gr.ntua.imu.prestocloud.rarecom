package gr.ntua.imu.prestocloud.rarecom.services


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
@ConfigurationProperties("application.context")
public class ContextService {

    Logger logger = LoggerFactory.getLogger(ContextService.class);

    String host
    String port


    RestTemplate restTemplate

//	private RestClient restClient;
	private Properties contextMappings;
	
	/**
	 * 
	 * @param host : ElasticSearch hostname or IP
	 * @param esPort : ElasticSearch port number
	 */
	public ContextService() {
		/***
		 * Documentation for low-level ES API in :
		 *  https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-low.html
		 ***/
		String result = "FAILED";
        restTemplate = new RestTemplate()


//		//tag::rest-client-init
//        this.restClient = RestClient.builder(new HttpHost(host, _esPort, "http")).build();
//        //end::rest-client-init


        contextMappings = new Properties();
        try {
            contextMappings.load(ContextService.class.getResourceAsStream("/context/mappings.properties"))
        } catch (Exception e) {
            logger.debug("loadContextMappings EXCEPTION ! ",e)
        }

        logger.debug(" [*] [CTX] Context Service Started ! '{}:{}'",host,port)

	}


	
	/***
	 * 
	 * @param conditionName : a condition declared in context-mappings.properties (property name)
	 * @return
	 */
	def evaluateContextCondition(String conditionName) {
		
		logger.debug(" [*] [CTX] Executing query with name : {}" , conditionName)
		
		String jsonString = this.contextMappings.getProperty(conditionName)
		
		System.out.println(" [*] [CTX] Executing query : {} " , jsonString)

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
        return restTemplate.postForEntity(
                "http://${host}:${port}/_all/search",
                entity,
                Object.class
        )

	}
	
	/***
	 * 
	 * @param conditionName : get the query string for a specific condition declared in context-mappings.properties
	 * @return
	 */
	String getContextConditionQuery(String conditionName) {
		return this.contextMappings.getProperty(conditionName);
	}


	/***
	 * 
	 * @param jsonString : evaluate an Elasticsearch query to /_all/_search
	 * @return
	 */
    def evaluateContextQuery(String jsonString) {
						

        System.out.println(" [*] [CTX] Executing query : " + jsonString);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);

        return restTemplate.postForEntity(
                "http:${host}:${port}/_all_search",
                entity,
                Object.class
        )


	}

}
