package gr.ntua.imu.prestocloud.rarecom.services.sdm

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.config.ApplicationConfiguration
import gr.ntua.imu.prestocloud.rarecom.domain.FeedbackAction
import gr.ntua.imu.prestocloud.rarecom.domain.FeedbackContext
import gr.ntua.imu.prestocloud.rarecom.services.DroolsContainerService
import gr.ntua.imu.prestocloud.rarecom.services.FeedbackActionService
import gr.ntua.imu.prestocloud.rarecom.services.FragmentService
import gr.ntua.imu.prestocloud.rarecom.services.SituationHistoryService
import gr.ntua.imu.prestocloud.rarecom.services.UiConfigService
import org.kie.api.runtime.KieSession
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class StandaloneFeedbackEvaluationService {

    Logger logger = LoggerFactory.getLogger(StandaloneFeedbackEvaluationService.class)


    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    SituationHistoryService situationHistoryService

    @Autowired
    FeedbackActionService feedbackActionService


    @Autowired
    DroolsContainerService droolsContainerService


    @Autowired
    FragmentService fragmentService


    @Autowired
    RarecomStaticRuleService rarecomStaticRuleService

    @Autowired
    UiConfigService uiConfigService

    @Autowired
    Gson gsonizer


    def evaluate(def situation) {

        logger.debug("Evaluating {} ",situation)

        if(!applicationConfiguration.feedback.enabled){
            logger.trace("Feedback service disabled")
            return
        }



        def time = situationHistoryService.timeToSituation(situation)
        def frequency = situationHistoryService.situationFrequency(situation)



        logger.debug("Evaluating \n\n time = {}\n frequency={} ",time, frequency)





        def context = new FeedbackContext()


        context.setTime(time)
        context.setFrequency(frequency)
        context.setTotalHosts(0)
        context.setCloudHosts(0)
        context.setEdgeHosts(0)
        context.setSituation(situation)


        if(context.act =='scale_in'){

            context.setMaxFrequency( applicationConfiguration.feedback.scaleInFrequency)
            context.setMaxTime(
                applicationConfiguration.feedback.scaleInFrequency * applicationConfiguration.cooldown
            )

        }

        if(context.act =='scale_out'){

            context.setMaxFrequency( applicationConfiguration.feedback.scaleOutFrequency)
            context.setMaxTime(
                applicationConfiguration.feedback.scaleOutFrequency * applicationConfiguration.cooldown
            )
        }



        def stats = fragmentService.getStatsForFragment(situation.fragid)


        if (stats != null) {
            context.setTotalHosts(stats.hosts.intValue())
            context.setCloudHosts(stats.cloud_hosts.intValue())
            context.setEdgeHosts(stats.edge_hosts.intValue())
        }

        logger.trace("Feedback context {} ",context)

        FeedbackAction action = new FeedbackAction()
        KieSession kieSession = droolsContainerService.getFeedbackContainer().newKieSession()
        kieSession.setGlobal('$a', action)
        kieSession.insert(context)
        kieSession.fireAllRules()
        kieSession.dispose()


        if (action.processed) {
            //alter rule

            feedbackActionService.record(action)
            logger.trace("Feedback matched {}",action)

            int instances = rarecomStaticRuleService.getInstancesForSituation(situation)

            if (action.increment > 0) {
                instances = stats.hosts * action.increment
            }

            instances++

            rarecomStaticRuleService.updateInstancesForSituation(instances, situation)

        } else {
            logger.trace("No feedback matched {}-{} ", time, gsonizer.toJson(situation))
        }

    }

}
