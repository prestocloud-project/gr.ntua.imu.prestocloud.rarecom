package gr.ntua.imu.prestocloud.rarecom.services.standalone

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.rarecom.amqp.PrestoCloudPublisher
import gr.ntua.imu.prestocloud.rarecom.domain.ADFAction
import gr.ntua.imu.prestocloud.rarecom.domain.SDMSituation
import gr.ntua.imu.prestocloud.rarecom.services.DistanceService
import gr.ntua.imu.prestocloud.rarecom.services.DroolsContainerService
import gr.ntua.imu.prestocloud.rarecom.services.SituationHistoryService
import org.kie.api.runtime.KieSession
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 05/10/18.
 */

@Service
class SituationHandlingService {

    Logger logger = LoggerFactory.getLogger(SituationHandlingService.class)

    @Autowired
    PrestoCloudPublisher prestoCloudPublisher

    @Autowired
    Gson gsonizer

    @Autowired
    DroolsContainerService droolsContainerService

    @Autowired
    SituationHistoryService situationHistoryService

    @Autowired
    FeedbackEvaluationService feedbackEvaluationService

    @Autowired
    DistanceService distanceService

    @PostConstruct
    void postConstruct(){

        situationHistoryService.clear();

    }

    void handleMessage(Object message){

        logger.debug("Handling {}",gsonizer.toJson(message))

        if(message == null){
            return
        }


        if( !(message instanceof Collection)){
            message = [message]
        }


        message.each{
            o ->

                SDMSituation sdmSituation = SDMSituation.build(o.event)

                ADFAction adfAction  = new ADFAction()

                try{


                    KieSession kieSession = droolsContainerService.getStaticContainer().newKieSession()
                    kieSession.setGlobal('$a', adfAction)
                    kieSession.setGlobal('$d', distanceService)
                    kieSession.insert(sdmSituation)
                    kieSession.fireAllRules()
                    kieSession.dispose()


                    if(adfAction.processed){

                        logger.debug("Matched {} {} ",sdmSituation,adfAction.matched)


                        logger.trace("Publishing action {} ",
                                gsonizer.toJson(adfAction))


                        prestoCloudPublisher.publish(
                                adfAction.topic,
                                gsonizer.toJson([
                                    "fragment_name":adfAction.fragmentName,
                                    "delta_instances": adfAction.deltaInstance
                                ])
                        )

                        feedbackEvaluationService.evaluate(adfAction,sdmSituation)
                        situationHistoryService.insertSituation(sdmSituation)

                    }else {
                        logger.debug("Taking no action {} {} ",sdmSituation,adfAction.matched)

                    }

                }catch(Exception e){
                    e.printStackTrace()
                    logger.warn("Couldn't handle situation {}",o)
                }finally{
                }
        }



    }
}
