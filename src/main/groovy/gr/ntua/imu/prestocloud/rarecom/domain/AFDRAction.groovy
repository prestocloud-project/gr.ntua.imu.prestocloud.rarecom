package gr.ntua.imu.prestocloud.rarecom.domain

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 26/10/18.
 */
class AFDRAction {

    String deltaInstances
    String fragmentName
    String timestamp

    String getDeltaInstances() {
        return deltaInstances
    }

    void setDeltaInstances(String deltaInstances) {
        this.deltaInstances = deltaInstances
    }

    String getFragmentName() {
        return fragmentName
    }

    void setFragmentName(String fragmentName) {
        this.fragmentName = fragmentName
    }

    String getTimestamp() {
        return timestamp
    }

    void setTimestamp(String timestamp) {
        this.timestamp = timestamp
    }
}
