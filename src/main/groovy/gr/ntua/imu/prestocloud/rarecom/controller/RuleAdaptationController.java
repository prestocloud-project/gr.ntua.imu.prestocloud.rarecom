package gr.ntua.imu.prestocloud.rarecom.controller;

import io.swagger.annotations.Api;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.kie.api.KieBase;
import org.kie.api.definition.KiePackage;
import org.kie.api.definition.rule.Rule;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@Api(value="/api",description="Rarecomm",produces ="application/json")

@RequestMapping("/api")
public class RuleAdaptationController {

	private KieSession kSession;

 	String getDroolsPackages() {
		String ret = "";
		Collection<KiePackage> packages = this.kSession.getKieBase().getKiePackages();
		for (KiePackage pkg : packages) {
			ret += "Package " + pkg.getName() + "\n";
			Collection<Rule> rules = pkg.getRules();
			for (Rule rule : rules) {
				ret += "\t" + rule.toString() + "\n";
			}
		}
		return "<pre>" + ret + "</pre>";
	}
	
	@RequestMapping(value = "/rules/add/{rulePackage}/{ruleName}", method = RequestMethod.POST , consumes="text/plain")
	String addRule(@PathVariable String rulePackage, @PathVariable String ruleName,
			@RequestBody String ruleText) {
		String errors;

		KieBase kbase = this.kSession.getKieBase();
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder(kbase);
		kbuilder.add(ResourceFactory.newByteArrayResource(ruleText.getBytes()), ResourceType.DRL);
		// Show errors
		if (kbuilder.hasErrors()) {
			errors = kbuilder.getErrors().toString();
			System.out.println(ruleText);
			System.out.println(errors);
			return "ERROR: " + errors;
		}
		((KnowledgeBaseImpl) this.kSession.getKieBase()).addPackages(kbuilder.getKnowledgePackages());
		return "SUCCESS";
	}
	
	@RequestMapping("/rules/delete/{rulePackage}/{ruleName}")
	String removeRule(@PathVariable String rulePackage, @PathVariable String ruleName) {
		// examples from :
		// https://github.com/kiegroup/drools/tree/7.7.x/drools-compiler/src/test/java/org/drools/compiler/integrationtests
		// KiePackage pkg = this.kSession.getKieBase().getKiePackage(rulePackage);
		try {
			this.kSession.getKieBase().removeRule(rulePackage, ruleName);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "FAILED: " + e.getMessage();
		}
		return "SUCCESS: removed + " + ruleName;
	}

}
