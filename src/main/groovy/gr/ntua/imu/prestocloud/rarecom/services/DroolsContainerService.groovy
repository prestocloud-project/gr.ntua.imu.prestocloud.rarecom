package gr.ntua.imu.prestocloud.rarecom.services

import org.kie.api.KieServices
import org.kie.api.builder.KieBuilder
import org.kie.api.builder.KieFileSystem
import org.kie.api.builder.KieModule
import org.kie.api.runtime.KieContainer
import org.kie.internal.io.ResourceFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-09-02.
 */
@Service
class DroolsContainerService {



    private static final String drlFile = "drools/static.drl";

    @Autowired
    RulePersistanceService rulePersistanceService

    KieContainer staticContainer
    KieContainer feedbackContainer

    @PostConstruct
    public post(){

        this.staticContainer = buildStaticContainer()
        this.feedbackContainer = buildFeedbackContainer()

    }

    KieContainer getStaticContainer() {
        return staticContainer
    }


    KieContainer getFeedbackContainer() {
        return feedbackContainer
    }


    public resetStaticContainer(){
        this.staticContainer  = buildStaticContainer()
    }



    private KieContainer buildStaticContainer(){

        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();

        File f = rulePersistanceService.generateFromConfig("./config/ui-config.json")
        kieFileSystem.write(ResourceFactory.newFileResource(f))
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem)
        kieBuilder.buildAll();
        KieModule kieModule = kieBuilder.getKieModule();
        return kieServices.newKieContainer(kieModule.getReleaseId());

    }


    private KieContainer buildFeedbackContainer() {


        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieFileSystem.write(ResourceFactory.newClassPathResource("drools/feedback.drl"));
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        kieBuilder.buildAll();
        KieModule kieModule = kieBuilder.getKieModule();
        return kieServices.newKieContainer(kieModule.getReleaseId());

    }
}
