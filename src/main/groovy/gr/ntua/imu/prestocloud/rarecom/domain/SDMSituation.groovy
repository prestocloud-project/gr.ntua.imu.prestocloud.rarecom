package gr.ntua.imu.prestocloud.rarecom.domain

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 05/10/18.
 */

class SDMSituation {

    Date ts
    String action
    String resource
    Map attributes
    String variable
    String fragid

    public static final SDMSituation  build(Object o ){

        def situation = new SDMSituation()
        situation.ts= new Date()
        situation.action = o.action
        situation.variable = o.res_inst
        situation.resource = o. rule_id
        situation.fragid = o.fragid
        situation.attributes=o.attributes

        return situation

    }

    Date getTs() {
        return ts
    }

    void setTs(Date ts) {
        this.ts = ts
    }

    String getAction() {
        return action
    }

    void setAction(String type) {
        this.action = type
    }

    String getResource() {
        return resource
    }

    void setResource(String resource) {
        this.resource = resource
    }

    Double getValue() {
        return value
    }

    void setValue(Double value) {
        this.value = value
    }

    String getVariable() {
        return variable
    }

    void setVariable(String variable) {
        this.variable = variable
    }

    String getFragid() {
        return fragid
    }

    void setFragid(String fragid) {
        this.fragid = fragid
    }


    @Override
    public String toString() {
        return "SDMSituation{" +
            "ts=" + ts +
            ", type='" + action + '\'' +
            ", resource='" + resource + '\'' +
            ", attributes=" + attributes +
            ", variable='" + variable + '\'' +
            ", fragid='" + fragid + '\'' +
            '}';
    }
}
