package gr.ntua.imu.prestocloud.rarecom.services

import org.springframework.stereotype.Service

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-01-31.
 */
@Service
class NormalizationService {

    public double normalize(v , min, max){


        /**
         *  Minmax normalization is a normalization strategy which
         *  linearly transforms x to y= (x-min)/(max-min), where min and max are
         *  the minimum and maximum values in X, where X is the set of observed
         *  values of x. This means, the minimum value in X is
         *  mapped to 0 and the maximum value in X is mapped to 1.
         */
        return (v-min)/(max-min)*100



    }

    public double greaterThan(v, threshold, max ){

        return ((v-threshold) / (max-threshold)) * 100

    }

    public double lessThan(v, threshold, min ){

        return ((v-threshold) / (threshold-min)) * 100

    }



}
