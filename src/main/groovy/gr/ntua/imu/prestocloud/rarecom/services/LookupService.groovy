package gr.ntua.imu.prestocloud.rarecom.services

import com.google.gson.Gson
import org.apache.commons.io.IOUtils
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-01-31.
 */
@Service
class LookupService {

    def table

    @PostConstruct
    post(){

        String o  =  IOUtils.toString(new FileInputStream("./config/lookup.json"),'UTF-8')

        table = new Gson().fromJson(o,List.class)
    }

    public def arches(dimensions,rules){

        return table.find({
            f->
                f.dimensions == dimensions && f.rules == rules
        })

    }
}
