package gr.ntua.imu.prestocloud.rarecom.services.sdm

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class StandaloneFeedbackCorrectionService {

    Logger logger = LoggerFactory.getLogger(StandaloneFeedbackCorrectionService.class)


    @Autowired
    RarecomStaticRuleService rarecomStaticRuleService


    @Scheduled(fixedDelay = 5000L)
    def correct(){

        return;
        def o = rarecomStaticRuleService.getAll('5s')

        o.each{
            rule ->
                def correctedValue = Math.ceil(rule.instances / 2)

                if(correctedValue < rule.zone){
                    correctedValue =rule.zone
                }

                
                logger.debug("Correcting Rule {}-{}({})- from {} to {} ",rule.id,rule.updated, rule.zone, rule.instances, correctedValue)
                rarecomStaticRuleService.updateInstance(
                    correctedValue,
                    rule.id
                )

        }

    }


}
