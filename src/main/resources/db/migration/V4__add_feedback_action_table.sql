set timezone TO 'Europe/Athens';

create table feedback_action(
    id serial,
    instances numeric,
    increment decimal(10,2),
    raw TEXT,
    occurred timestamp
);

create unique index feedback_action_pkey ON feedback_action(id,occurred desc);


