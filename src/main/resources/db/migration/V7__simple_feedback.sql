create table simple_feedback(
  id serial primary key ,
  zone varchar(255),
  fragment varchar(255),
  instances decimal(10,2),
  raw TEXT,
  occurred timestamp
);

