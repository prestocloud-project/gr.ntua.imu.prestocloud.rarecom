set timezone TO 'Europe/Athens';

create table situation(
  id serial,
  type varchar(255),
  hash VARCHAR(512),
  fragid varchar(100),
  value  DOUBLE PRECISION  NULL,
  raw TEXT,
  occurred timestamp
);

create unique index situation_pkey ON situation(id,occurred desc);


