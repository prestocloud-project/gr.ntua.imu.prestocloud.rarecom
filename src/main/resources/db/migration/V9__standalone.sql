create table static_rules(

                             id serial,
                             fragid VARCHAR(255),
                             action VARCHAR(255),
                             zone SMALLINT,
                             instances INT,
                             updated TIMESTAMP
);



create table annotations(

                            id serial,
                            action VARCHAR(255),
                            text VARCHAR(255),
                            occurred TIMESTAMP


);

SELECT create_hypertable('annotations', 'occurred');
