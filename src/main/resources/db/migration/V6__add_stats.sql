create table stats(
  id serial,
  k varchar(255),
  v varchar(255),
  ret varchar(255),
  occurred timestamp
);

create unique index stats_pkey ON stats(id,occurred desc);
