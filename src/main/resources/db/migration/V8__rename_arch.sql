alter table simple_feedback rename to archs;


create table simple_feedback(
      id serial primary key ,
      zone varchar(255),
      fragment varchar(255),
      instances decimal(10,2),
      occurred timestamp
);

alter table archs drop column instances;
