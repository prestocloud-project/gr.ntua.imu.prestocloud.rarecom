create table fragment(
  id serial primary key ,
  name VARCHAR(255),
  hosts NUMERIC,
  cloud_hosts NUMERIC,
  edge_hosts NUMERIC,
  raw TEXT,
  stored timestamp
);
