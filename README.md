# Overview

The Resource Adaptation Recommender (RARecom) is the software tool that aims to reason about adaptation actions within the PrestoCloud infrastructure. During the second iteration of the tool we focused on creating a clear separation between when an adaptation recommendation needs to be triggered and what the recommendation is, and how it affects the overall infrastructure.

This specific version of the tool can function in 2 ways. One considered "standalone" where the logic to discover _severity zones_ is self contained. 
And the second is considered "coupled" where the logic to determine _severity zones_ is performed by the SDM.

All communication is done through the Message Broker, and the message topic depends on whether we are in "standalone" or "coupled" mode.


## What you need

In order to run the component in either mode you will need:

1. Java 1.8
2. Gradle
3. Docker
4. Docker Compose 


## Standalon Instructions

First you need to run docker in order to initialze Timescale[1], the Postgresql based Time Series database. 

```
$ cd docker/infr
$ docker-compose start
```

This will start the docker images, once this is done run 

```

$ docker ps

```

You should see the following

```
CONTAINER ID        IMAGE                                            COMMAND                  CREATED             STATUS              PORTS                      NAMES
30b575fbd7a8        grafana/grafana                                  "/run.sh"                5 months ago        Up About an hour    0.0.0.0:3000->3000/tcp     infr_graph_1
15ebe81aa1f3        timescale/timescaledb:latest-pg9.6               "docker-entrypoint.s…"   5 months ago        Up About an hour    0.0.0.0:12345->5432/tcp    infr_postgres_1
```

Now you are ready to start RARecom. RARecom is a Spring Boot applicaiton, which uses gradle.
From the root of the project, run 

```
$ ./gradlew bootRun
```

This will clean, build the project and run the application.

### Configuring RAREcom

Take a look at the files under the [config/](https://gitlab.com/prestocloud-project/gr.ntua.imu.prestocloud.rarecom/tree/master/config) folder.

1. [application.yml]((https://gitlab.com/prestocloud-project/gr.ntua.imu.prestocloud.rarecom/blob/master/config/application.yml))

This is the Spring Boot configuration file. The RAREcom specific configuration options are under the _application_ key 

2. [ui-config.json]((https://gitlab.com/prestocloud-project/gr.ntua.imu.prestocloud.rarecom/blob/master/config/ui-config.json))

In the *standalone* mode, the developer needs to configure the scalability rules during runtime. A sample of these rules is 
included in the file.








